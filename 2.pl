suspected(luc).
suspected(paul).
suspected(alain).
suspected(bernard).
suspected(louis).
murdered(jean).

solve(X) :- murderer(X,jean).
%	X is a murderer if he's suspected and has a motivation to kill Y and he has a gun and he has no proof
%murderer(X,Y) :- suspected(X), hasmotive(X,Y), hasgun(X), hasnoproof(X), murdered(Y).
murderer(X,Y) :- suspected(X), hasmotive(X,Y), hasgun(X), hasnoproof(X), murdered(Y).
%	X has no proof if Y has claim and Y is a reliable person
hasproof(X) :- claim(X,Y), reliableperson(Y).
hasnoproof(X) :- not(hasproof(X)).
%	X has a motivation to kill Y if X has reason to kill Y or if X wants to revange from Y
hasmotive(X,Y) :- hasreason(X,Y); torevenge(X,Y).
%	X has a reason to kill Y if he heir from Y or he debt to Y or Y has seen a crime scene
hasreason(X,Y) :- heir(X,Y); debtor(X,Y); crimescene(X,Y).

claim(luc,bernard).
claim(paul,bernard).
claim(louis,luc).

%not reliableperson(alain).
reliableperson(luc).
reliableperson(paul).
reliableperson(bernard).
reliableperson(louis).

torevenge(paul,jean).
torevenge(luc,jean).
heir(bernard,jean).
heir(jean,louis).
debtor(louis,jean).
debtor(luc,jean).
crimescene(alain,jean).
hasgun(luc).
hasgun(louis).
hasgun(alain).