parent(john,ann).
parent(jim,john).
parent(jim,keith).
parent(mary,ann).
parent(mary,sylvia).
parent(brian,sylvia).
male(john).
male(keith).
male(jim).
male(brian).
female(sylvia).
female(ann).
female(mary).

neq(X,Y):- X\=Y.
negsex(X,Y) :- (male(X), female(Y)) ; (female(X), male(Y)).
brother(X,Y) :- male(X), parent(Z,X), parent(Z,Y), neq(X,Y).
uncle(X,Y) :- parent(Z,Y), male(Z), brother(X,Z).
%halfsister(X,Y) :- female(X), female(Y), ((parent(Z,X), parent(Z,Y), male(Z), parent(T,X), parent(V,Y), female(T), female(V), neq(T,V)); (parent(ZZ,X), parent(ZZ,Y), female(ZZ), parent(TT,X), parent(VV,Y), male(VV) , male(TT), neq(TT,VV))).
halfsister(X,Y) :- female(X), female(Y), parent(Z,X), parent(Z,Y), parent(V1,X), parent(V2,Y), negsex(Z,V1), negsex(Z,V2), neq(V1,V2).