create_bst(R) :- Tree = [R, [], []], nb_setval(bst, Tree).

insert_bst(X) :- nb_getval(bst, T), insert(T, X, Tree), nb_setval(bst, Tree).

%insert(givenTree,X,TakenTree)
insert([], X, [X, [], []]).
insert([I, L, R], X, [I, LL, R]) :- X < I, insert(L, X, LL).
insert([I, L, R], X, [I, LL, R]) :- X == I, insert(L, X, LL).
insert([I, L, R], X, [I, L, RR]) :- X > I, insert(R, X, RR).

show_bst(T) :- nb_getval(bst, T).
